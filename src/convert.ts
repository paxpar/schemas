import { compile, compileFromFile } from 'json-schema-to-typescript'

// compile from file
compileFromFile('../public/checklist-1.0.schema.json')
    .then(ts => fs.writeFileSync('checklist.d.ts', ts))

// or, compile a JS object
let mySchema = {
    properties: [...]
}
compile(mySchema, 'MySchema')
    .then(ts => ...)