# paxpar public schemas

Published at https://paxpar.gitlab.io/schemas

## use via poetry

Add this section to your `pyproject.toml` :

```ini
[[tool.poetry.source]]
name = "gitlab-pp-schemas"
url = "https://gitlab.com/api/v4/projects/32901859/packages/pypi/simple"
```

or

```ini
[[tool.poetry.source]]
name = "gitlab-pp-schemas"
url = "https://__token__:${MY_TOKEN}@gitlab.com/api/v4/projects/32901859/packages/pypi/simple"
```

Then :
```yaml
poetry add paxpar-schemas
```

## use via pip

```bash
export MY_TOKEN=xxxxx
pip install paxpar-schemas --extra-index-url https://__token__:${MY_TOKEN}@gitlab.com/api/v4/projects/32901859/packages/pypi/simple
```

## testing

```yaml
poetry run pytest -v
```

## generate schema

To generate the JSON schemas :

```bash
poetry run python -m paxpar_schemas.gen
```

## setup

Requirements :
* python >= 3.8
* poetry

```bash
poetry install
```

## publish new version


```bash
# generate JSON schemas
poetry run python -m paxpar_schemas.gen
# bump version
poetry version patch
```

Commit and push.

A gitlab ci/cd pipeline will be triggered.

## update in target project

If the target project use poetry,
you can update to the newest version with :

```bash
poetry update paxpar-schemas
```

## yaml header

To ease auto completion add on the first line of YAML files :

Example For a checklist :
```yaml
# yaml-language-server: $schema=https://paxpar.gitlab.io/schemas/checklist-1.0.schema.json
```

Example for a craftform :
```yaml
# yaml-language-server: $schema=https://paxpar.gitlab.io/schemas/craftform-1.0.schema.json
```

## TODO

* [ ] generate [typescript interfaces from JSON schema](https://github.com/bcherny/json-schema-to-typescript)
* [ ] publish typescript interfaces as an npm package in [gitlab package registry](https://docs.gitlab.com/ee/user/packages/npm_registry/)
* [ ] also publish to schemas.paxpar.tech
