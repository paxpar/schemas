# generate typescript

Generate typescript from json-schema using [json-schema-to-typescript](https://github.com/bcherny/json-schema-to-typescript)


```
json2ts -i 'schemas/**/*.json'
```