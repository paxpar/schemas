from pydantic import BaseModel
import json
from . import models



def gen_schema(model: BaseModel, name: str, version: str):
    filename = f'public/{name}-{version}.schema.json'
    print(f'generating {filename} ...')
    # this is equivalent to json.dumps(MainModel.schema(), indent=2):
    #print(MainModel.schema_json(indent=2))
    #json_schema = model.schema_json( indent=2)
    json_schema = model.model_json_schema()
    # set "$schema": "http://json-schema.org/draft-07/schema#",
    json_schema['$schema'] = "http://json-schema.org/draft-07/schema#"
    # set "$id": "https://gitlab.com/paxpar/schemas/-/raw/main/schemas/checklist-1.0.schema.json",
    json_schema['$id'] = f"https://gitlab.com/paxpar/schemas/-/raw/main/{filename}"
    json_schema_raw = json.dumps(json_schema, indent=2)
    open(filename, 'w').write(json_schema_raw)


if __name__ == "__main__":
    gen_schema(models.craftform.CraftForm, 'craftform', '1.0')
    gen_schema(models.checklist.v1.Checklist, 'checklist', '1.0')
    gen_schema(models.checklist.v2.Checklist, 'checklist', '2.0')
    #gen_schema(gen_checklist_2.Checklist, 'checklist', '2.0')
    gen_schema(models.ludi.cards.Deck, 'deck', '1.0')
    gen_schema(models.menus.Menus, 'menus', '1.0')
    gen_schema(models.plans.Plans, 'plans', '1.0')
    gen_schema(models.roles.Roles, 'roles', '1.0')
    gen_schema(models.conf.Conf, 'conf', '1.0')
