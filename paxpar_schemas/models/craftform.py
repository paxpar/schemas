from enum import Enum
from typing import List, Optional
from pydantic import BaseModel, RootModel, Extra
from .base import BaseRef


class BillingOption(BaseModel, extra="forbid"):
    id: str
    name: str
    desc: Optional[str] = None
    credits: Optional[int] = 0
    factice_allowed: Optional[bool] = False
    question: Optional[str] = None
    include: Optional[List[str]] = []
    tags: Optional[List[str]] = []
    mandatory: bool = False
    enabled: bool = True


BillingOptions = RootModel[List[BillingOption]]


class Billing(BaseModel, extra="forbid"):    
    credits: Optional[int] = 0
    factice_allowed: Optional[bool] = False
    options: BillingOptions


class Template(BaseModel, extra="forbid"):    
    id: str
    name: str
    filename: str
    custom: bool = False


class TemplateSet(BaseModel, extra="forbid"):    
    id: str
    desc: Optional[str] = None
    templates: Optional[List[Template]] = None


class WizardMessages(BaseModel, extra="forbid"):    
    '''
    Messages shown in the craftform wizard
    '''
    step_drop_file: Optional[str] = None
    step_input: Optional[str] = None
    step_check: Optional[str] = None
    step_finalize: Optional[str] = None
    step_finalized: Optional[str] = None


class Wizard(BaseModel, extra="forbid"):    
    messages: Optional[WizardMessages] = None


# see https://github.com/samuelcolvin/pydantic/discussions/4321
#class CraftForm(BaseRef, extra="forbid"):
class CraftForm(BaseRef):
    """
    This is the description of the main model
    """

    preview: bool = False
    dropzone_msg: Optional[str] = None
    billing: Optional[Billing] = None
    template_sets: Optional[List[TemplateSet]] = None
    wizard_component: Optional[str] = None
    wizard: Optional[Wizard] = None
    # the checklist to use in the craftform wizard to check before finalize
    checklist: Optional[str] = None
    # list of schema that will be accepted on wizard when dropping PDF
    form_overload_schemas: Optional[List[str]] = None
    dropfile_webcam: bool = False
