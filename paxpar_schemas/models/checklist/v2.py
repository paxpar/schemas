from typing import List, Union, Any, Optional
from pydantic import BaseModel, Field, Extra
from datetime import datetime

from paxpar_schemas.models.base import BaseRef, Theme


# Don't do extra="forbid" beacause we have specific fields
class CheckStep(BaseModel):
    '''
    Every step in a checklist is a CheckStep
    '''
    id: Optional[str] = ''
    title: str
    desc: Optional[str] = ''
    icon: Optional[str] = "mdi:help"
    #status: Optional[str]
    status: Optional[str] = None
    module: Optional[str] = None
    args: Any = None
    #TODO: enforce children to be Node
    children: Optional[list] = []
    factice: Optional[bool] = False
    show: str | None = None
    assert2: Optional[Any] = Field(alias="assert", default=None)


# -------------------------------------------------------

class FilterConditionSignatureArgs(BaseModel, extra="forbid"):
    pubkey: str

class FilterConditionSignature(BaseModel, extra="forbid"):
    signature: FilterConditionSignatureArgs
    score_hit: int = 80

# -------------------------------------------------------

class FilterConditionMetadataArgs(BaseModel, extra="forbid"):
    schema2: str = Field(alias="$schema")

class FilterConditionMetadata(BaseModel, extra="forbid"):
    metadata: FilterConditionMetadataArgs
    score_hit: int = 230

# -------------------------------------------------------

class FilterConditionAttachmentArgs(BaseModel, extra="forbid"):
    name: str

class FilterConditionAttachment(BaseModel, extra="forbid"):
    attachment: FilterConditionAttachmentArgs
    score_hit: int = 40

# -------------------------------------------------------

class FilterConditionXmpArgs(BaseModel, extra="forbid"):
    name: str

class FilterConditionXmp(BaseModel, extra="forbid"):
    xmp: FilterConditionXmpArgs
    score_hit: int = 40

# -------------------------------------------------------

class FilterConditionFilename(BaseModel, extra="forbid"):
    filename: str
    score_hit: int = 20


FilterCondition = FilterConditionSignature | \
        FilterConditionMetadata | \
        FilterConditionAttachment | \
        FilterConditionXmpArgs | \
        FilterConditionFilename

# -------------------------------------------------------

#TODO: add extra.forbid when issue fixed
# see https://github.com/samuelcolvin/pydantic/discussions/4321
#class Checklist(BaseRef, extra="forbid"):
class Checklist(BaseRef):
    '''
    The checklist
    Is has some props missing that will be computed later    

    $schema: https://paxpar.tech/schema/checklist/1
    '''

    filter: Optional[dict] = None
    filter2: List[FilterCondition] = []
    steps: List[CheckStep]
    default: bool = False
    # infoPdf: SignInfoAllModel


# -------------------------------------------------------
