from typing import List, Literal, Union, Any, Optional
from pydantic import BaseModel, Extra, Field
from datetime import datetime


class Theme(BaseModel):
    name: Optional[str] = None

    dark: bool = True
    # set primary or color1
    primary: Optional[str] = None
    color1: Optional[str] = None
    # set secondary or color2
    secondary: Optional[str] = None
    color2: Optional[str] = None
    accent: Optional[str] = None
    error: Optional[str] = None
    info: Optional[str] = None
    success: Optional[str] = None
    warning: Optional[str] = None

    logo_small: Optional[str] = None
    logo_text: Optional[str] = None
    logo_big: Optional[str] = None

    verify_document_caption: Optional[str] = None


class Owner(BaseModel, extra="forbid"):
    signature_pubkey: Optional[str] = None
    signature: Optional[str] = None
    # Only the email is mandatory
    email: str


class GalleryActionRef(BaseModel):
    """
    name: custom form
    icon: mdi-home
    url: /front/craftform/custom/med
    """

    url: str
    name: Optional[str] = None
    icon: Optional[str] = None


class GalleryRef(BaseModel):
    """
    actions:
        - name: custom form
          icon: mdi-home
          url: /front/craftform/custom/med
    """

    # show in gallery
    show: bool = True
    # acces enabled from gallery
    enabled: bool = True
    # not published on public gallery
    published: bool = False
    published_order: Optional[float] = None
    lab: bool = False
    # list of restricted domains (no restriction if empty)
    private: List[str] = []
    actions: List[GalleryActionRef] = []


BaseRefVisible = Literal["author", "hidden", "disabled"]


# see https://github.com/samuelcolvin/pydantic/discussions/4321
# class BaseRefPublic(BaseModel, extra="forbid"):
class BaseRefPublic(BaseModel):
    """
    Public attributes of a ref object.
    """

    # don't use _ as first field name !!
    # cf # see https://github.com/samuelcolvin/pydantic/discussions/4321
    # _schema: Optional[str] = Field(alias="$schema", default=None)
    schema2: Optional[str] = Field(alias="$schema", default=None)
    name: Optional[str] = None
    desc: Optional[str] = None
    image: Optional[str] = None
    # TODO: image2 will be deprecated when 0.x will be migrated to 1.x
    image2: Optional[str] = None
    icon: Optional[str] = None
    tags: List[str] = []
    version: Optional[str | float] = None
    author: Optional[str | List[str]] = None
    portal: Optional[str | List[str]] = None
    visible: Optional[BaseRefVisible] = None
    # TODO: deprecate gallery
    gallery: Optional[GalleryRef] = None


class BaseRef(BaseRefPublic):
    """
    $schema: https://paxpar.tech/schema/craft/1
    title: Relance et Mise-en-demeure
    image: https://media.paxpar.tech/med_craft.jpeg
    desc: Formulaire de création de lettre de relance ou mise-en-demeure.
    tags:
    - sponsor
    - dev
    gallery:
        actions:
            - name: custom form
              icon: mdi-home
              url: /front/craftform/custom/med
    """

    source: Optional[str] = None
    owner: Optional[Owner] = None
    theme: Optional[Theme] = None


class BaseRefTyped(BaseRefPublic):
    """
    BaseRef with a type.
    Usefull to get list of ref objects with their type
    """

    id: str
    type: str
