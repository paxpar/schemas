from enum import Enum
from typing import List, Optional, Union
from pydantic import BaseModel, Extra, RootModel




class Role(BaseModel, extra="forbid"):
    """
    This is the description of a single role
    """

    id: str
    desc: Optional[str]


Roles = RootModel[List[Role]]
