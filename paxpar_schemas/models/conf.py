from dataclasses import field
from enum import Enum
from typing import Any, List, Optional
from pydantic import BaseModel, Field, Extra, JsonValue
from pydantic_settings import BaseSettings

from .base import BaseRef


class JwtConf(BaseModel):
    # HS256 or RS256
    algo: str
    # token expiration in minutes
    expire: int
    # public/secret keys for RS256 algo
    # pk == sk == secret for HS256 algo
    # use configmap to store this multi-line value
    pk: str
    # SECRET : This is a sensible value, you must provide the real value elsewhere
    sk: str


class SMTP(BaseModel, extra="forbid"):
    server: str
    port: int
    login: str


# stripe payment credentials
class Stripe(BaseModel, extra="forbid"):
    # public key
    pk: str
    # secret key
    # SECRET : This is a sensible value, you must provide the real value elsewhere
    sk: str
    # webhook secret
    webhookSecret: str
    redirect_url: str
    api_version: str


class CorsConf(BaseModel):
    domains: List[str] = []
    domains_full: List[str] = []


class Ressource(BaseModel, extra="forbid"):
    memory: Optional[str] = None
    cpu: Optional[str] = None


class Ressources(BaseModel, extra="forbid"):
    requests: Optional[Ressource] = None
    limits: Optional[Ressource] = None


class BasicService(BaseModel, extra="forbid"):
    replica: int
    image: str
    port: int
    resources: Ressources


class IngressItem(BaseModel):
    host: str

class Ingress(BaseModel, extra="forbid"):
    enabled: bool
    annotations: dict

    # if no hosts section, there will be no ingress
    # give hosts values in each deploy gitops repo
    hosts: List[IngressItem] = []
    #  - host: prod.paxpar.tech
    #  - host: uat.paxpar.tech
    #  - host: dev.paxpar.tech
    #  - host: paxpar.tech


class ServicesConf(BaseModel, extra="forbid"):
    conv: BasicService
    core: BasicService
    dss: BasicService
    forge: BasicService
    store: BasicService


class PscYousignConf(BaseModel):
    api_key: str
    api_url: str
    app_url: str


class PscConf(BaseModel):
    yousign: PscYousignConf


class ServicesCoreConf(BaseModel):
    craft_api: bool = False
    filemanager_api: bool = False
    info_api: bool = False
    mycheck_api: bool = False
    openapi_gotenberg: bool = False
    auth_api: bool = False
    backoffice_api: bool = False
    ref_api: bool = False
    sign_api: bool = False
    sign_cmd_api: bool = False


class RefSyncConf(BaseModel):
    # sync ref S3 on service startup
    on_start: bool = True
    freq: int = 10  # seconds
    timeout: int = 600  # seconds


# TODO: deprecate in favor of RefSource
class S3cred(BaseModel):
    endpoint_url: str
    region_name: str
    signature_version: str = "s3v4"
    access_key: str
    secret_key: str
    bucket_name: str


class S3sources(BaseModel):
    ref: S3cred
    session: S3cred


class RefSource(BaseModel):
    path_prefix: str | None = None
    path_root: str | None = None
    fsspec: dict[str, JsonValue] | None = None


class RefConf(BaseModel):
    sources: dict[str, RefSource] = field(default_factory=lambda: {})
    sync: RefSyncConf
    base_path: Optional[str] = None


class PrometheusConf(BaseModel):
    # Prometheus API (victoria API)
    # Example : "http://localhost:8428"
    # Example : "http://192.168.8.165:8428"
    # Example : "http://82.64.35.2:8428"
    api_url: str
    # endpoints where pp service publish metrics
    expose_endpoint: Optional[str] = "/api/metrics"

class SupabaseConf(BaseModel):
    url: str
    key: str


class SubscribeMailConf(BaseModel):
    to: str
    title: str
    sleep: int
    message: str


class SmtpConf(BaseModel):
    server: str
    port: int = 587
    username: str
    password: str
    starttls: bool = True
    ssl_tls: bool = False
    use_credentials: bool = True
    validate_certs: bool = True


class MailConf(BaseModel):
    from_email: str
    from_name: str
    subscribe: SubscribeMailConf
    smtp: SmtpConf


class Conf(BaseSettings, extra="forbid"):
    core_routers: ServicesCoreConf
    cors: CorsConf
    jwt: JwtConf
    mail: MailConf
    prometheus: PrometheusConf
    psc: PscConf
    ref: RefConf
    s3: S3sources
    supabase: SupabaseConf
    services: ServicesConf
    #stripe: Stripe

    image: Optional[dict] = {}
    imagePullSecrets: Optional[List] = []
    nameOverride: Optional[str] = None
    fullnameOverride: Optional[str] = None
    serviceAccount: Optional[dict] = {}
    podSecurityContext: Optional[dict] = {}
    securityContext: Optional[dict] = {}
    service: Optional[dict] = {}
    ingress: Ingress
    nodeSelector: Optional[dict] ={}
    tolerations: Optional[List] = []
    affinity: Optional[dict] = {}
    # enabled/disable the liveness and readiness probes
    livenessReadinessProbe: Optional[bool] = True
