from enum import Enum
from typing import List, Optional, Union
from pydantic import BaseModel, Extra


class Plan(BaseModel, extra='forbid'):
    """
    This is the description of a single Plan
    """

    id: str
    desc: Optional[str] = None


class Plans(BaseModel, extra='forbid'):
    """
    This is the description of a list of Plans
    """

    plans: List[Plan] = None
