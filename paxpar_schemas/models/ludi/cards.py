from enum import Enum
from typing import List, Optional, Union
from pydantic import BaseModel, Extra



class Card(BaseModel, extra="forbid"):
    """
    This is the description of a deck of cards
    """

    id: Union[int, List[int]]
    enabled: Optional[bool]
    only: Optional[bool]
    title: Optional[str]
    picto: Optional[str]
    desc: Optional[str]
    page: Optional[str]
    tags: Optional[List[str]]
    chain: Optional[List[str]]
    template_recto: Optional[str]
    template_verso: Optional[str]
    pdf: Optional[bool]



class Deck(BaseModel, extra="forbid"):
    """
    This is the description of a deck of cards
    """

    cards: List[Card]
