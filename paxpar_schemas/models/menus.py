from enum import Enum
from typing import List, Optional, Union
from pydantic import BaseModel, Extra, RootModel




class Menu(BaseModel, extra="forbid"):
    """
    This is the description of a single Menu
    """
    title: str
    icon: Optional[str] = None
    to: Optional[str] = None


class Divider(BaseModel, extra="forbid"):
    divider: bool


class Header(BaseModel, extra="forbid"):
    header: str


Menus = RootModel[List[Union[Menu,Divider,Header]]]
