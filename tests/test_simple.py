from paxpar_schemas import models

def test_checklist_module():
    assert 'checklist' in dir(models)


def test_checklist_class():
    assert 'Checklist' in dir(models.checklist)


def test_craftform():
    assert 'craftform' in dir(models)

