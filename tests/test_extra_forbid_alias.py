import pytest
from pydantic import BaseModel, Extra, Field
from pydantic import ValidationError


class Boo0(BaseModel):
    aaa: str
    bbb: str
    #_ccc: str = Field(alias="ccc")
    ccc_hidden: str = Field(alias="ccc")

class Boo1(BaseModel, extra="forbid"):
    aaa: str
    bbb: str
    ccc2: str = Field(alias="ccc")

class Boo2(BaseModel, populate_by_name=True):
    aaa: str
    bbb: str
    ccc2: str = Field(alias="ccc")

class Boo3(BaseModel, extra="forbid", populate_by_name=True):
    aaa: str
    bbb: str
    ccc2: str = Field(alias="ccc")


class Boo4(BaseModel, populate_by_name=False):
    aaa: str
    bbb: str
    ccc2: str = Field(alias="ccc")


class Boo5(BaseModel, extra="forbid", populate_by_name=False):
    aaa: str
    bbb: str
    ccc2: str = Field(alias="ccc")


def test_boo_all():
    print()
    print('                  Boo0, Boo1, Boo2, Boo3, Boo4, Boo5')
    print('aaa,bbb,ccc      ', end='')
    for Boo in (Boo0, Boo1, Boo2, Boo3, Boo4, Boo5):
        try:
            c = Boo(**{
                'aaa': 'aaaaaa',
                'bbb': 'bbbbbb',
                'ccc': 'cccccc',
            })
            print('  ok  ', end='')
        except:
            print('  err ', end='')
    print()
    print('aaa,bbb,ccc,ddd  ', end='')
    for Boo in (Boo0, Boo1, Boo2, Boo3, Boo4, Boo5):
        try:
            c = Boo(**{
                'aaa': 'aaaaaa',
                'bbb': 'bbbbbb',
                'ccc': 'cccccc',
                'ddd': 'dddddd',
            })
            print('  ok  ', end='')
        except:
            print('  err ', end='')
    print()

def test_boo():

    # see https://github.com/samuelcolvin/pydantic/discussions/4321
    c = Boo1(**{
        'aaa': 'aaaaaa',
        'bbb': 'bbbbbb',
        'ccc': 'cccccc',
    })

    # fails as expected, ddd is obviously an extra field
    with pytest.raises(ValidationError):    
        c = Boo1(**{
            'aaa': 'aaaaaa',
            'bbb': 'bbbbbb',
            'ccc': 'cccccc',
            'ddd': 'dddddd',
        })


    # fails as expected, ccc2 is not known
    with pytest.raises(ValidationError):    
        c = Boo1(**{
            'aaa': 'aaaaaa',
            'bbb': 'bbbbbb',
            'ccc2': 'cccccc',
        })

