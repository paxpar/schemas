import pytest
from pydantic import BaseModel, Extra, Field
from pydantic.error_wrappers import ValidationError



class Model1(BaseModel, extra="forbid"):
    aaa: str
    bbb2: str = Field(alias="bbb")


class Model2(Model1, extra="forbid"):
    ccc2: str = Field(alias="ccc")



def test_010():

    c = Model2(**{
        'aaa': 'aaaaaa',
        'bbb': 'bbbbbb',
        'ccc': 'cccccc',
    })

    # fails as expected, ddd is obviously an extra field
    with pytest.raises(ValidationError):    
        c = Model2(**{
            'aaa': 'aaaaaa',
            'bbb': 'bbbbbb',
            'ccc': 'cccccc',
            'ddd': 'dddddd',
        })

    # fails as expected, bbb is missing
    with pytest.raises(ValidationError):    
        c = Model2(**{
            'aaa': 'aaaaaa',
            'ccc': 'cccccc',
        })

