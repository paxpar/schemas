import pytest
from pydantic import BaseModel, Extra, Field
from pydantic.error_wrappers import ValidationError


def test_010():

    class Model1(BaseModel, extra="forbid"):
        aaa: str
        bbb2: str = Field(alias="bbb")


    class Model2(Model1, extra="forbid"):
        ccc2: str = Field(alias="ccc")

    c = Model2(**{
        'aaa': 'aaaaaa',
        'bbb': 'bbbbbb',
        'ccc': 'cccccc',
    })

    # alias are not used by default
    assert c.model_dump() == {
        "aaa": "aaaaaa",
        "bbb2": "bbbbbb",
        "ccc2": "cccccc",
    }

    # to get aliases we must use `by_alias`
    assert c.model_dump(by_alias=True) == {
        "aaa": "aaaaaa",
        "bbb": "bbbbbb",
        "ccc": "cccccc",
    }
